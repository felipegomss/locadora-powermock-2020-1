package br.ucsal.testequalidade20192.locadora;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import static org.powermock.api.mockito.PowerMockito.*;

import br.ucsal.testequalidade20192.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20192.locadora.dominio.*;
import br.ucsal.testequalidade20192.locadora.persistence.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({LocacaoBO.class, ClienteDAO.class,VeiculoDAO.class, LocacaoDAO.class})
public class LocacaoBOUnitarioTest {

	/**
	 * Verificar se ao locar um veículo disponível para um cliente cadastrado, um
	 * contrato de locação é inserido.
	 * 
	 * Método:
	 * 
	 * public static Integer locarVeiculos(String cpfCliente, List<String> placas,
	 * Date dataLocacao, Integer quantidadeDiasLocacao) throws
	 * ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
	 * VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado
	 *
	 * Observação1: lembre-se de mocar os métodos necessários nas classes
	 * ClienteDAO, VeiculoDAO e LocacaoDAO.
	 * 
	 * Observação2: lembre-se de que o método locarVeiculos é um método command.
	 * 
	 * @throws Exception
	 */
	
	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {
		
		//Criando Objetos
		Cliente cliente = new Cliente("11111111111", "Caju", "999999999");
		Modelo modelo = new Modelo("Corsa");
		Veiculo veiculo = new Veiculo("AXJ-7953", 2005, modelo, 15.90);
		String placa = "AXJ-7953";
		Date data = new Date();
		
		//Criando as listas e adicionando os objetos
		List<Veiculo> veiculos = new ArrayList<>();
		List<String> placas = new ArrayList<>();
		veiculos.add(veiculo);
		placas.add(placa);
		
		//Criando uma nova loca��o
		Locacao locacao = new Locacao(cliente, veiculos, data, 5);
		
		//Mockando as classes 
		mockStatic(ClienteDAO.class);
		when(ClienteDAO.obterPorCpf("11111111111")).thenReturn(cliente);		
		mockStatic(VeiculoDAO.class);
		when(VeiculoDAO.obterPorPlaca("AXJ-7953")).thenReturn(veiculo);		
		mockStatic(LocacaoDAO.class);
		whenNew(Locacao.class).withAnyArguments().thenReturn(locacao);
		LocacaoBO.locarVeiculos("11111111111", placas, data, 5);

		//Verificando as informa��es dos m�todos 
		verifyStatic(ClienteDAO.class);
		ClienteDAO.obterPorCpf("11111111111");
		verifyStatic(VeiculoDAO.class);
		VeiculoDAO.obterPorPlaca("AXJ-7953");
		verifyStatic(LocacaoDAO.class);
		LocacaoDAO.insert(locacao);
		verifyNoMoreInteractions(LocacaoBO.class);

	}
}
